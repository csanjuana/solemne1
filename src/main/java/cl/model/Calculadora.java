package cl.model;


public class Calculadora 
{
    private float c;
    private float i;
    private float a;

    public Calculadora(float c, float i, float a)
    {
        this.c = c;
        this.i = i;
        this.a = a;
    }
    
    public float CalcularInteres()
    {
        float I = (this.c * (this.i/100)) * this.a;
        return I;
    }    

    public float getC() {
        return c;
    }

    public void setC(float c) {
        this.c = c;
    }

    public float getI() {
        return i;
    }

    public void setI(float i) {
        this.i = i;
    }

    public float getA() {
        return a;
    }

    public void setA(float a) {
        this.a = a;
    }


    
}
